#include <iostream>
#include <fstream>

//Untuk menggunakan fungsi sleep()
#include <unistd.h>

//Untuk menggunakan fungsi getch()
#include <conio.h>

//Untuk menggunakan fungsi setw();
#include <iomanip>

//Untuk atoi di Pembayaran()
#include <stdlib.h>

//untuk deteksi waktu kapan pembelian barang
#include <ctime>

//Untuk convert angka ke string
#include <sstream>

#define delay Sleep

//untuk fungsi gotoxy
#include <windows.h>
using namespace std;


void MainMenu();

COORD coord = {X: 0, Y: 0};
void gotoxy(int x, int y){
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}


void Opening(){
	int x_pos = 33;
	gotoxy(x_pos,5); 	cout<<"||--------------------------------------------------------||"<<endl;
	gotoxy(x_pos,6);	cout<<"||--------------------------------------------------------||"<<endl;
	gotoxy(x_pos,7);	cout<<"||                     .|''''                             ||"<<endl;
	gotoxy(x_pos,8);	cout<<"||                       | |                              ||"<<endl;
	gotoxy(x_pos,9);	cout<<"||                       |'|            ._____            ||"<<endl;
	gotoxy(x_pos,10);	cout<<"||               ___    |  |            |.   |' .---''    ||"<<endl;
	gotoxy(x_pos,11);	cout<<"||       _    .-'   '-. |  |     .--'|  ||   | _|    |    ||"<<endl;
	gotoxy(x_pos,12);	cout<<"||    .-'|  _.|  |    ||   '-__  |   |  |    ||      |    ||"<<endl;
	gotoxy(x_pos,13);	cout<<"||    |' | |.    |    ||       | |   |  |    ||      |    ||"<<endl;
	gotoxy(x_pos,14);	cout<<"||____|  '-'     '    ''       '-'   '-.'    '`      |____||"<<endl;
	gotoxy(x_pos,15);	cout<<"||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||"<<endl;
	gotoxy(x_pos,16); 	cout<<"||--------------------------------------------------------||"<<endl;
	for(int i=x_pos-1;i<61; i++){
		gotoxy(i,17);	cout <<"=================================";
		gotoxy(i,18);	cout <<"| SELAMAT DATANG DI K-BANG MART |";
		gotoxy(i,19);	cout <<"=================================";
		delay(75);
		gotoxy(i,17);cout << " ";
		gotoxy(i,18);cout << " ";
		gotoxy(i,19);cout << " ";
		
    }
}

void hiasan(){
	system("color 17");
	char a=177, b=219;
	int timing = 10;
	
	int atas_bawah =30, kiri_kanan = 119;
	
	//atas
	for (int i=0;i<kiri_kanan; i++){
		gotoxy(i,0);
		cout << b;
		Sleep(timing);
 	}
 	
 	//bawah
	for (int i=kiri_kanan-1; i>=0; i--){
		gotoxy(i,atas_bawah-1);
		cout << b;
		Sleep(timing);
 	}
 	
 	//kanan
	for (int i=0;i<atas_bawah; i++){
		gotoxy(kiri_kanan,i);
		cout << b;
		Sleep(timing);
	}
	
 	
 	//kiri
	for (int i=atas_bawah-1; i>= 0 ; i--){
		gotoxy(0,i);
		cout << b;
		Sleep(timing);
 	}
}


void hiasan1(){
	system("color 17");
	char a=177, b=219;
	
	int atas_bawah =30, kiri_kanan = 119;
	
	//atas
	for (int i=0;i<kiri_kanan; i++){
		gotoxy(i,0);
		cout << b;
 	}
 	
 	//kanan
	for (int i=0;i<atas_bawah; i++){
		gotoxy(kiri_kanan,i);
		cout << b;
	}
	
	//bawah
	for (int i=kiri_kanan-1; i>=0; i--){
		gotoxy(i,atas_bawah-1);
		cout << b;
 	}
 	
 	//kiri
	for (int i=atas_bawah-1; i>= 0 ; i--){
		gotoxy(0,i);
		cout << b;
 	}
}


struct produk{
	string nama;
	int harga, stok;
	produk* next;
	produk* prev;
};

struct keranjang{
	string nama;
	int jumlah, harga;
	keranjang* next;
};

struct pembeli{
	string nama, jenis;
	int jumlah, harga, jam, menit, hari, bulan, tahun;
	pembeli* next;
	pembeli* prev;
	keranjang* HeadKrj = NULL;
};

pembeli *HEAD_Pembeli = NULL, *TAIL_Pembeli = NULL;
produk *HEAD_Makanan=NULL, 			*TAIL_Makanan = NULL;
produk *HEAD_Minuman=NULL, 			*TAIL_Minuman = NULL;
produk *HEAD_Obat=NULL, 			*TAIL_Obat = NULL;
produk *HEAD_ATK=NULL, 				*TAIL_ATK = NULL;
produk *HEAD_Elektronik=NULL, 		*TAIL_Elektronik = NULL;
produk *HEAD_Perabot=NULL, 			*TAIL_Perabot = NULL;
produk *HEAD_PeralatanBayi=NULL, 	*TAIL_PeralatanBayi = NULL;

pembeli riwayat[1000];

int jumlah_riwayat = 0;
bool sortNama_Makanan = 0, sortNama_Minuman = 0, sortNama_Obat = 0,sortNama_ATK = 0,sortNama_Perabot = 0, sortNama_Elektronik = 0, sortNama_PltBayi = 0;
int reset;

void insertDoubleLast(produk* x,produk** HEAD, produk** TAIL){
	if(*HEAD == NULL){
		(*HEAD) = x;
		(*TAIL) = x;
	} else {
		x->prev = (*TAIL);
		(*TAIL)->next = x;
		(*TAIL) = x;
	}
}
void deleteFirst(produk** HEAD){
	produk* temp = (*HEAD)->next;
	(*HEAD) = NULL;
	(*HEAD) = temp;
}


void insertLast(pembeli* x){
	if(HEAD_Pembeli == NULL){
		HEAD_Pembeli = x;
		TAIL_Pembeli = x;
	} else {
		x->prev = TAIL_Pembeli;
		TAIL_Pembeli->next = x;
		TAIL_Pembeli = x;
	}
}

void deleteDoubleFirst(produk** HEAD, produk** TAIL){
	if( (*HEAD) == (*TAIL)){
		(*HEAD) = NULL;
	}else{
		produk* temp = (*HEAD);
		(*HEAD) = (*HEAD)->next;
		(*HEAD)->prev = NULL;
		temp->next = NULL;
		temp = NULL;
		delete temp;	
	}
}

void deleteDoubleLast(produk** HEAD, produk** TAIL){
	produk* pointer = (*HEAD);
	
	if( (*HEAD) == (*TAIL)){
		deleteDoubleFirst(HEAD, TAIL);
	} else{
		produk* temp = (*TAIL);
		(*TAIL) = (*TAIL)->prev;
		(*TAIL)->next = NULL;
		
		temp->next = NULL;
		temp->prev = NULL;
		temp=NULL;
		delete temp;
	}
}

void deleteDoubleAt(int indeks,produk** HEAD){
	produk* pointer = (*HEAD);
	produk* temp;
	for(int i=1; i < indeks-1; i++){
		pointer = pointer->next;
	}
	temp=pointer->next;
	pointer->next=temp->next;
	(temp->next)->prev = pointer;
	
	temp = NULL;
	delete temp;
}

produk* getProduk(int indeks,produk* HEAD){
	produk* pointer = HEAD;
	for(int i=0; i < indeks-1; i++){
		pointer = pointer->next;
	}
	return pointer;
}

produk* updateProduk(int indeks,produk** HEAD){
	produk* pointer = (*HEAD);
	for(int i=0; i < indeks-1; i++){
		pointer = pointer->next;
	}
	return pointer;
}


void DeteksiResetRiwayat(){
	time_t sekarang = time(0);
	struct tm *jam = localtime(&sekarang);
	
	int hari = jam-> tm_mday;
	int bulan = 1 + jam-> tm_mon;
	int tahun = 1900 + jam-> tm_year;
	
	static int t[] = { 0, 3, 2, 5, 0, 3, 
                       5, 1, 4, 6, 2, 4 };  
    tahun -= bulan < 3;  
    int day = ( tahun + tahun / 4 - tahun / 100 +  tahun / 400 + t[bulan - 1] + hari) % 7;  
    
    
    string hari_sekarang = 	day == 0? "Minggu":
		    				day == 1? "Senin":
		    				day == 2? "Selasa":
			    			day == 3? "Rabu":
		    				day == 4? "Kamis":
		   					day == 5? "Jum'at":
		    				day == 6? "Sabtu":"";
		    				
    if ( hari_sekarang == "Senin" && reset == 0) {
    	for ( int i = 0; i < 1000; i++ ){
    		riwayat[i].nama 	= "";
			riwayat[i].jenis 	= "";
			riwayat[i].harga 	= 0;
			
			riwayat[i].hari 	= 0;
			riwayat[i].bulan 	= 0;
			riwayat[i].tahun 	= 0;
			
			riwayat[i].jam 		= 0;
			riwayat[i].menit	= 0;
		}
		
		jumlah_riwayat = 0;
		
		reset = 1;	
	} else if ( hari_sekarang != "Senin" ){
		reset = 0;
	}
}


void ReadProdukLoadout(string filename, produk** HEAD, produk** TAIL){
	//Fungsi baca file produk
	string ambil, direktori = "barang/" + filename + ".txt";
	int index=1, stok, harga;
	
	fstream file_produk;
	file_produk.open(direktori.c_str(), ios::in); 
	
	produk* produkBaru = new produk();
	while(getline(file_produk, ambil)){ 
		if(index % 4== 1 ){ 
			produkBaru->nama = ambil;
			
		} else if(index % 4== 2 ){ 
			stok = atoi(ambil.c_str()); 
			produkBaru->stok  = stok;
			
		} else if(index % 4 == 3){ 
			harga = atoi(ambil.c_str()); 
			produkBaru->harga  = harga;
			
			insertDoubleLast(produkBaru,HEAD,TAIL);
			produkBaru = new produk();
			
		}
		index++;
	}
	file_produk.close();
	
}

void Loadout(){
	//Berisi produk barang & riwayat pembelian yg di-load ke struct saat awal program
	string ambil;
	int index, jumlah, harga, jam, menit, hari, bulan, tahun;
	
	ReadProdukLoadout("file_makanan", &HEAD_Makanan, &TAIL_Makanan);
	ReadProdukLoadout("file_minuman", &HEAD_Minuman, &TAIL_Minuman);
	ReadProdukLoadout("file_obat", &HEAD_Obat, &TAIL_Obat);
	ReadProdukLoadout("file_atk", &HEAD_ATK, &TAIL_ATK);
	ReadProdukLoadout("file_perabot", &HEAD_Perabot, &TAIL_Perabot);
	ReadProdukLoadout("file_elektronik", &HEAD_Elektronik, &TAIL_Elektronik);
	ReadProdukLoadout("file_peralatanbayi", &HEAD_PeralatanBayi, &TAIL_PeralatanBayi);

	fstream file_riwayat_pembeli;
	fstream file_riwayat_barang;

	file_riwayat_pembeli.open		("riwayat/file_riwayat_pembeli.txt", ios::in);
	file_riwayat_barang.open		("riwayat/file_riwayat_barang.txt", ios::in);
	
	
	index = 1;
	pembeli* baru = new pembeli();
	while(getline(file_riwayat_pembeli, ambil)){ 
		
		if(index % 9 == 1 ){ 
			baru->nama = ambil;
			
		} else if(index % 9 == 2 ){ 
			jumlah = atoi(ambil.c_str()); 
			baru->jumlah = jumlah;
			
		} else if(index % 9 == 3){ 
			harga = atoi(ambil.c_str()); 
			baru->harga  = harga;
			
		} else if(index % 9 == 4){ 
			hari = atoi(ambil.c_str()); 
			baru->hari = hari;
			
		} else if(index % 9 == 5){ 
			bulan = atoi(ambil.c_str()); 
			baru->bulan = bulan;
			
		} else if(index % 9 == 6){ 
			tahun = atoi(ambil.c_str()); 
			baru->tahun = tahun;
			
		} else if(index % 9 == 7){ 
			jam = atoi(ambil.c_str()); 
			baru->jam = jam;
			
		} else if(index % 9 == 8){ 
			menit = atoi(ambil.c_str()); 
			baru->menit = menit;
			
		} else if(index % 9 == 0){ 
			reset = atoi(ambil.c_str()); 
			insertLast(baru);
			baru = new pembeli();
	
		}  
		index++;
	}
	
	
	index = 1;
	keranjang* baruKrj = new keranjang();
	pembeli* ptrPembeli;

	while(getline(file_riwayat_barang, ambil)){
		if(ambil.find("=====INDEXBARU=====") != string::npos){
			ptrPembeli = (index == 1) ? HEAD_Pembeli : ptrPembeli->next;
			index++;
			
		} else if(ambil.find("Jumlah") != string::npos){ 
			jumlah = atoi(ambil.c_str()); 
			baruKrj->jumlah = jumlah;
			
		} else if(ambil.find("Harga") != string::npos){ 
			harga = atoi(ambil.c_str()); 
			baruKrj->harga = harga;
			if( ptrPembeli->HeadKrj == NULL ){
				ptrPembeli->HeadKrj = baruKrj;
				ptrPembeli->HeadKrj->next = NULL;
			}else{
				baruKrj->next = NULL;
				keranjang* ptr = ptrPembeli->HeadKrj;
				while(ptr->next != NULL){
					ptr=ptr->next;
				}
				ptr->next = baruKrj;
			}
			baruKrj = new keranjang();
			
		} else { 
			baruKrj->nama = ambil;
			
		}	
		
	} 

	file_riwayat_pembeli.close();
	file_riwayat_barang.close();
}


void SaveProdukFunction(string filename, produk* HEAD){
	//Fungsi baca file produk
	string ambil, direktori = "barang/" + filename + ".txt";
	int index=1, stok, harga;
	
	fstream file_produk;
	file_produk.open(direktori.c_str(), ios::out | ios::trunc); 
	
	produk* ptr = HEAD; 
    while (ptr != NULL) { 
    	file_produk << ptr->nama  <<endl;
		file_produk << ptr->stok << "  stok" <<endl;
		file_produk << ptr->harga  << " harga" <<endl;
		file_produk << "=====[Batas Index]=====" <<endl;
        ptr = ptr->next; 
    } 
	file_produk.close();
}

void SaveDataOnExit(){
	//Simpan data produk & riwayat sebelum logout ( Admin )
	
	SaveProdukFunction("file_makanan", HEAD_Makanan);
	SaveProdukFunction("file_minuman", HEAD_Minuman);
	SaveProdukFunction("file_obat", HEAD_Obat);
	SaveProdukFunction("file_atk", HEAD_ATK);
	SaveProdukFunction("file_perabot", HEAD_Perabot);
	SaveProdukFunction("file_elektronik", HEAD_Elektronik);
	SaveProdukFunction("file_peralatanbayi", HEAD_PeralatanBayi);
	
	
	fstream file_riwayat_pembeli;
	fstream file_riwayat_barang;

	file_riwayat_pembeli.open		("riwayat/file_riwayat_pembeli.txt", ios::out | ios::trunc);
	file_riwayat_barang.open		("riwayat/file_riwayat_barang.txt", ios::out | ios::trunc);
	
	pembeli* ptr = HEAD_Pembeli;
	keranjang* ptrKeranjang; 
    while (ptr != NULL) { 
		file_riwayat_pembeli << ptr->nama  <<endl;
		file_riwayat_pembeli << ptr->jumlah << " : [Jumlah Barang]" <<endl;
		file_riwayat_pembeli << ptr->harga  <<endl;
		file_riwayat_pembeli << ptr->hari  <<endl;
		file_riwayat_pembeli << ptr->bulan  <<endl;
		file_riwayat_pembeli << ptr->tahun  <<endl;
		file_riwayat_pembeli << ptr->jam  <<endl;
		file_riwayat_pembeli << ptr->menit <<endl;
		file_riwayat_pembeli << reset << " : Keadaan Reset=====[Batas Index]=====" <<endl;
		
		file_riwayat_barang << "=====INDEXBARU=====" << endl;
		ptrKeranjang = ptr->HeadKrj;
		while (ptrKeranjang != NULL) {
			file_riwayat_barang << ptrKeranjang->nama << endl;
			file_riwayat_barang << ptrKeranjang->jumlah << " Jumlah" << endl;
			file_riwayat_barang << ptrKeranjang->harga << " Harga" << endl;
			ptrKeranjang = ptrKeranjang->next;
		}
		
        ptr = ptr->next; 
    } 

	file_riwayat_pembeli.close();
	file_riwayat_barang.close();
}



int min(int x, int y) { 
	return (x<=y)? x : y; 
	} 
  

int fibonacciSearch(produk arr[], string x, int n){ 
    // Inisialisasi angka fibonacci yang ke-0 dan ke-1
    int fib_m2 = 0;   // Fibonacci Fm-2
    int fib_m1 = 1;   // Fibonacci Fm-1
    int fib_m = fib_m2 + fib_m1;  // Fibonacci yang ke - m 
  
    // Generate angka fibonacci hingga > n
    while (fib_m < n) { 
        fib_m2 = fib_m1; 
        fib_m1 = fib_m; 
        fib_m  = fib_m2 + fib_m1; 

    } 
  
    // Penanda batas pencarian
    int batas = -1; 
  
    // Angka yang ingin dicari akan diperiksa di tiap arr [indeks] hingga fib_m sisa 1, yang berarti fib_n2 = 0 dan fib_n1 = 1
    while (fib_m > 1) { 
        // Menentukan index berdasarkan angka fibonacci
        int i = min(batas+fib_m2, n-1); 
        
    	//Jika angka x lebih besar daripada arr[i], maka array dari batas ke index i diabaikan, Fm menjadi F (m-1)
        if (arr[i].nama < x) { 
            fib_m  = fib_m1; 
            fib_m1 = fib_m2; 
            fib_m2 = fib_m - fib_m1; 
            batas = i; 
        } 
  	
        //Jika angka x lebih kecil daripada arr[i], maka array setelah index i diabaikan, Fm menjadi F (m-2)
        else if (arr[i].nama > x) { 
            fib_m  = fib_m2; 
            fib_m1 = fib_m1 - fib_m2; 
            fib_m2 = fib_m - fib_m1; 
        } 
        
		// Jika ketemu, kembalikan nilai index
		else {	
        	return i;
		}
    } 
    
    // Kembalikan nilai index array dari bilangan yang dicari jika ditemukan
    if ( arr[batas+1].nama == x && (batas+1) < n ){
    	return batas+1; 
	} 
	
	// Kembalikan nilai -1 jika tidak ditemukan
	else {
		return -1; 
	} 
}

//QUICK SORT
void quickPartitionSwap(produk* x, produk* y){
	produk temp = *x;
	*x = *y;
	*y = temp;
}

int partisi( produk data[], string dataCompare[], int low, int high, int urutan){
	int i = (low - 1);
	
	//pivot atau nlai acuan dalam quick sort
	string pivot = dataCompare[high];
	for (int j = low; j <= high - 1; j++){
		if(urutan == 1){
			if (dataCompare[j] < pivot){
				i++;
				swap(dataCompare[i], dataCompare[j]);
				quickPartitionSwap(&data[i], &data[j]);
			}
		} else if(urutan == 2){
			if (dataCompare[j] > pivot){
				i++;
				swap(dataCompare[i], dataCompare[j]);
				quickPartitionSwap(&data[i], &data[j]);
			}
		}	
	}
	swap(dataCompare[i + 1], dataCompare[high]);
	quickPartitionSwap(&data[i +1], &data[high]);
	return (i + 1);
}

void quickSort(produk data[], string dataCompare[], int low, int high, int urutan){
	if (low < high){
		int pi = partisi(data, dataCompare, low, high, urutan);
		
		quickSort(data, dataCompare, low, pi - 1, urutan);
		quickSort(data, dataCompare, pi + 1, high, urutan);
	}

}

void prosesSorting(produk data[], int jumlah, string jenis, int urutan){
	string dataCompare[jumlah];

	for (int i = 0; i < jumlah; i++){
		if (jenis == "nama"){
			dataCompare[i]= data[i].nama;
		} else if (jenis == "harga"){
			stringstream ss_data;
			ss_data<<( data[i].harga );
			ss_data>> dataCompare[i];
		}
	}
	
	quickSort(data, dataCompare, 0, jumlah - 1, urutan);
}

void assignLinkedListToArray(produk* HEAD, produk* TAIL, produk data[]){
	produk* pointer = HEAD;
	
	int i = 0;
	while(pointer != TAIL->next){
		data[i] = *pointer;
		i++;
		pointer = pointer->next;
	}
}

void assignArrayToLinkedList(produk** HEAD, produk** TAIL, produk data[]){
	int i = 0;
	(*HEAD)->nama = data[i].nama;
	(*HEAD)->stok = data[i].stok;
	(*HEAD)->harga = data[i].harga;
	produk *pointer = (*HEAD)->next;
	i++;
	
	while( pointer != (*TAIL)->next){
		pointer->nama = data[i].nama;
		pointer->stok = data[i].stok;
		pointer->harga = data[i].harga;
		
		i++;
		pointer = pointer->next;
		
	}
}


int length(produk* head){ 
    produk* curr = head; 
    int count = 0; 
    while (curr != NULL) { 
        count++; 
        curr = curr->next; 
    } 
    return count; 
} 




void RiwayatPembeli(pembeli* ptrPembeli){
	system("cls");
 	system("color 17");
 	
 	int x_pos = 8, y_pos = 7;
 	gotoxy(x_pos, 2); cout << "+=======================================================================================================+" << endl;
	gotoxy(x_pos, 3); cout << "|" << string(32, '-') <<	"Riwayat Pembeli Mini Market K-BANG MART"	<< string(32, '-') << "|" << endl;
	gotoxy(x_pos, 4); cout << "+=============+================================+==============================+=======+=================+" << endl;
	gotoxy(x_pos, 5); cout << "| No. Antrian |          Nama pembeli          |      Barang yang dibeli      |  Qty. |      Harga      |" << endl;
	gotoxy(x_pos, 6); cout << "+=============+================================+==============================+=======+=================+" << endl;
	
	if(HEAD_Pembeli == NULL){
		cout << "|                                          DATA PEMBELI KOSONG                                          |" << endl;
	} else {
		int i = 1;
		while ( ptrPembeli != NULL) {
			gotoxy(x_pos, y_pos++); cout << "|" << setw(11)<< i << ". | " << setw(30)	<< ptrPembeli->nama <<" |" 
			<< setw(31) <<"|" << setw(8) << "|"  << setw(18) << " |" << endl;
			
			keranjang* ptrKeranjang = ptrPembeli->HeadKrj;
			while ( ptrKeranjang  != NULL) {
				gotoxy(x_pos, y_pos++); 
				cout << "|" << setw(14) << "|" << setw(33) << "|" 
					<< setw(29) << ptrKeranjang->nama << " |"
					<< setw(6) << ptrKeranjang->jumlah << " | Rp. "  << setw(11) << ptrKeranjang->harga << " |" << endl;	
				ptrKeranjang = ptrKeranjang->next;
			}
			
			gotoxy(x_pos, y_pos++); cout << "|             +--------------------------------+------------------------------+-------+-----------------+" << endl;
			gotoxy(x_pos, y_pos++); cout << "|" << setw(33) << 	"| Pembelian tanggal " << setw(3) << ptrPembeli->hari << setw(3) << "-" 
				<< setw(3) << ptrPembeli->bulan << setw(3)<< "-" << setw(5) << ptrPembeli->tahun
				<< setw(6) << " Jam " << setw(3) << ptrPembeli->jam << setw(3) << ":" << setw(3) << ptrPembeli->menit  
				<< setw(25) << "| TOTAL | Rp." << setw(12) << ptrPembeli->harga << " |"<<endl;
			gotoxy(x_pos, y_pos++); cout << "+-------------+--------------------------------+------------------------------+-------+-----------------+" << endl;
			ptrPembeli = ptrPembeli->prev;
			i++;
		}
		
	}
	
	
	gotoxy(x_pos, y_pos++); cout << "|"		<< string(38, '-') 	<<	" Tekan untuk melanjutkan.. "	<<  string(38, '-')  << 	  "|" << endl;
	gotoxy(x_pos, y_pos++); cout << "+=======================================================================================================+" << endl;
	getch();
	
}

int DaftarProduk(string kategori){
	system("cls");
	system("color 17");
	
	int y_pos = 8, i;
	produk* ptr;
	
	gotoxy(23, 2); cout << "+======+====================================+======+===================+" << endl;
	gotoxy(23, 3); cout << "|" << string(17, '-') <<	"Daftar Produk Minimarket K-BANG MART"	 << string(17, '-') << "|" << endl;
	gotoxy(23, 4); cout << "+======+====================================+======+===================+" << endl;
	gotoxy(23, 5); cout << "| No.  |             Nama produk            | Stok |       Harga       |" << endl;
	gotoxy(23, 6); cout << "+======+====================================+======+===================+" << endl;
	gotoxy(23, 7); cout << "|"					<< 		string(70, ' ') 				<<		   "|" << endl;
	
	if (kategori == "makanan" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Makanan;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Makanan                                                              |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "minuman" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Minuman;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Minuman                                                              |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "obat" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Obat;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Obat-obatan                                                          |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "atk" || kategori == "admin"){
		i = 1;
		ptr = HEAD_ATK;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Alat Tulis Kerja                                                     |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "perabot" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Perabot;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Perabotan Rumah                                                      |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "elektronik" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Elektronik;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Alat / Perkakas Elektronik                                           |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "peralatanbayi" || kategori == "admin"){
		ptr = HEAD_PeralatanBayi;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Peralatan Bayi                                                       |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}  
	
	
	gotoxy(23, y_pos++); cout << "+======+====================================+======+===================+" << endl << endl;
	gotoxy(23, y_pos++); cout << "|"		<< string(23, '-') 	<<	" Tekan untuk melanjutkan "	<<  string(22, '-')  << 	  "|" << endl;
	gotoxy(23, y_pos++); cout << "+" 				<< 	string(70, '=') 		<< "+" << endl;
	getch();
	return y_pos;
}

int RincianProduk(string kategori,int i){
	system("cls");
	int jumlah_stok;
	gotoxy(23, 2); cout << "+===================================+==================================+" << endl;
	
	if (kategori == "makanan") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_Makanan)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "minuman") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_Minuman)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "obat") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_Obat)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "atk") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_ATK)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "perabot") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_Perabot)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "elektronik") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_Elektronik)->nama <<" |"<< setw(35) << "|" << endl;
	} else if (kategori == "peralatanbayi") {
		gotoxy(23, 3); cout << "| " << setw(33)  << getProduk(i,HEAD_PeralatanBayi)->nama <<" |"<< setw(35) << "|" << endl;
	}

	gotoxy(23, 4); cout << "+-----------------------------------+----------------------------------+" << endl;
	if (kategori == "makanan") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_Makanan)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_Makanan)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_Makanan)->stok;
	} else if (kategori == "minuman") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_Minuman)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_Minuman)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_Minuman)->stok;
	} else if (kategori == "obat") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_Obat)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_Obat)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_Obat)->stok;
	} else if (kategori == "atk") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_ATK)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_ATK)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_ATK)->stok;
	} else if (kategori == "perabot") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_Perabot)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_Perabot)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_Perabot)->stok;
	} else if (kategori == "elektronik") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_Elektronik)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_Elektronik)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_Elektronik)->stok;
	} else if (kategori == "peralatanbayi") {
		gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << getProduk(i,HEAD_PeralatanBayi)->harga 	<< " | Stok: "  <<	setw(26)  << getProduk(i,HEAD_PeralatanBayi)->stok <<" |" << endl;
		jumlah_stok = getProduk(i,HEAD_PeralatanBayi)->stok;
	}
	
	gotoxy(23, 6); cout << "+===================================+==================================+" << endl;
	
	return jumlah_stok;
}


void MessageLoading( string pesan, int titik){
	system("cls");
	string titik_loading =  (titik == 1 ) ? ".   ":
							(titik == 2 ) ? "..  ":
							(titik == 3 ) ? "... ":
							(titik == 4 ) ? "....":"";
	gotoxy (23,14); cout << "+"<< string(70, '=') << "+" << endl;
	if ( pesan == "login" ){
		gotoxy (23,15); cout << "|                       Autentikasi Login Admin" << titik_loading << "                    |" << endl;
	}
	
	else if ( pesan == "tambah" ){
		gotoxy (23,15); cout << "|                          Menambahkan Produk" << titik_loading << "                      |" << endl;
	}
	else if ( pesan == "cari" ){
		gotoxy (23,15); cout << "|                            Mencari Produk" << titik_loading << "                        |" << endl;
	}
	else if ( pesan == "ubah" ){
		gotoxy (23,15); cout << "|                         Mengubah Data Produk" << titik_loading << "                     |" << endl;
	}
	else if ( pesan == "hapus" ){
		gotoxy (23,15); cout << "|                           Menghapus Produk" << titik_loading << "                       |" << endl;
	}
	else if ( pesan == "exit" ){
		gotoxy (23,15); cout << "|                         Keluar Dari Aplikasi" << titik_loading << "                     |" << endl;
	}
	
	 
	gotoxy (23,16); cout << "+"<< string(70, '=') << "+" << endl;
}

void Loading(string pesan){
	for ( int i = 0; i < 3; i++){
		MessageLoading(pesan,1);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,2);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,3);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,4);
		for ( int j = 0; j < 1e8 ; j++){}
	}	
}

void Message(int a){
	system("cls");
	gotoxy (23,13); cout << "+"<< string(70, '=') << "+" << endl;
	gotoxy (23,14); cout << "|                                                                      |" << endl;
	if (a==1){
		gotoxy (23,15); cout << "|              Maaf pilihan yang anda masukkan tidak benar             |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==3){
		gotoxy (23,15); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==4){
		gotoxy (23,15); cout << "|              Identitas tidak cocok dengan data pada ATM!             |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} else if (a==5){
		gotoxy (23,15); cout << "|                         Saldo ATM tidak cukup                        |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} else if (a==6){
		gotoxy (23,15); cout << "|                  Metode Pembayaran tidak terdeteksi!                 |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} 
	
	
	//Pesan Create
	else if (a == 10){
		gotoxy (23,15); cout << "|                      Penambahan produk Berhasil                      |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	}
	else if (a == 11){
		gotoxy (23,15); cout << "|                                ERROR!                                |" << endl;
		gotoxy (23,16); cout << "|             Input Harga salah / tidak dapat dibawah Rp. 1            |" << endl;
	}
	else if (a == 12){
		gotoxy (23,15); cout << "|                          Input Stok salah!!                          |" << endl;
		gotoxy (23,16); cout << "|                      Range input stok: 1 - 9999                      |" << endl;
	}
	
	//Pesan Update
	else if (a == 20){
		gotoxy (23,15); cout << "|                      Data produk berhasil diubah                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	} 
	//Pesan Delete
	else if (a == 30){
		gotoxy (23,15); cout << "|                      Penghapusan produk Berhasil                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	}
	
	//Pesan admin
	
	else if (a == 77){
		gotoxy (23,15); cout << "|                     Produk yang dicari tidak ada                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	}
	
	//Login
	else if (a==88){
		gotoxy (23,15); cout << "|                         Password anda salah                          |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==89){
		gotoxy (23,15); cout << "|                 Anda tidak dapat login sebagai admin                 |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==90){
		gotoxy (23,15); cout << "|                         Anda berhasil login                          |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==99){
		gotoxy (23,15); cout << "|    Terima kasih telah menggunakan aplikasi Minimarket K-BANG MART    |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	}
	 
	gotoxy (23,17); cout << "+"<< string(70, '=') << "+" << endl;
}


int KategoriProduk(){
	int input, x_pos = 28;
	string input_kategori;
	system("cls");
	hiasan1();
	gotoxy(x_pos, 2);  cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 3);  cout << "|                          KATEGORI PRODUK                             |" << endl;
	gotoxy(x_pos, 4);  cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 5);  cout << "| 1. Makanan                                                           |" << endl;
	gotoxy(x_pos, 6);  cout << "| 2. Minuman                                                           |" << endl;
	gotoxy(x_pos, 7);  cout << "| 3. Obat-obatan                                                       |" << endl;
	gotoxy(x_pos, 8);  cout << "| 4. Alat Tulis Kerja                                                  |" << endl;
	gotoxy(x_pos, 9);  cout << "| 5. Perabotan Rumah                                                   |" << endl;
	gotoxy(x_pos, 10); cout << "| 6. Alat / Perkakas Elektronik                                        |" << endl;
	gotoxy(x_pos, 11); cout << "| 7. Peralatan Bayi                                                    |" << endl;
	gotoxy(x_pos, 12); cout << "+----------------------------------------------------------------------+" << endl;
	gotoxy(x_pos, 13); cout << "| 8. Kembali ke menu                                                   |" << endl;
	gotoxy(x_pos, 14); cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 15); cout << " Masukan pilihan anda : "; 
	cin >> input_kategori;
	fflush(stdin);
	input = atoi(input_kategori.c_str()); 
	if ( !(input > 0 && input < 9) ){
		Message(1);
		sleep(2);
	}
	return input;
}


void TambahProduk(){
	int stok_int, harga_int, pilih = KategoriProduk(), x_pos = 35;
	string nama, stok_string, harga_string;
	
	system("cls");
	hiasan1();
	if ( pilih >  0 && pilih < 6){
		gotoxy(x_pos, 10); cout << "+----------------------------------------------------+" << endl;
		gotoxy(x_pos, 11);	cout << "|                   Tambah Produk                    |";
		gotoxy(x_pos, 12); cout << "+----------------------------------------------------+" << endl;
		gotoxy(x_pos, 13); cout << "  Nama produk       : "; 
		getline (cin, nama);
		
		gotoxy(x_pos, 14); cout << "  Harga produk      : Rp. "; 
		getline (cin, harga_string);
		
		gotoxy(x_pos, 15); cout << "  Jumlah stok       : "; 
		getline (cin, stok_string);
		
		stok_int  = atoi ( stok_string.c_str());
		harga_int = atoi ( harga_string.c_str());

		if ( harga_int < 1 ){
			Message(11);
			sleep(2);
			
		} else if ( stok_int < 1 || stok_int > 9999 ){
			Message(12);
			sleep(2);
			
		} else {
			Loading("tambah");
			produk* produkBaru = new produk();
			produkBaru->nama = nama;
			produkBaru->stok = stok_int;
			produkBaru->harga = harga_int;
			produkBaru->next = NULL;
			produkBaru->prev = NULL;
			if ( pilih == 1){
				insertDoubleLast(produkBaru, &HEAD_Makanan, &TAIL_Makanan);
				sortNama_Makanan = false;
			} else if ( pilih == 2){
				insertDoubleLast(produkBaru, &HEAD_Minuman, &TAIL_Minuman);
				sortNama_Minuman = false;
			} else if ( pilih == 3){
				insertDoubleLast(produkBaru, &HEAD_Obat, &TAIL_Obat);
				sortNama_Obat = false;
			} else if ( pilih == 4){
				insertDoubleLast(produkBaru, &HEAD_ATK, &TAIL_ATK);
				sortNama_ATK = false;
			}else if ( pilih == 5){
				insertDoubleLast(produkBaru, &HEAD_Perabot, &TAIL_Perabot);
				sortNama_Perabot = false;
			} else if ( pilih == 6){
				insertDoubleLast(produkBaru, &HEAD_Elektronik, &TAIL_Elektronik);
				sortNama_Elektronik = false;
			} else if ( pilih == 7){
				insertDoubleLast(produkBaru, &HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
				sortNama_PltBayi = false;
			}
			Message(10);
			sleep(3);
		}
	}
}


int sortirProduk(produk** HEAD, produk** TAIL, produk data[], int jumlah, string jenis, int urutan, bool* sortStatus){
	
	//Urutan 1: asc, urutan 2: desc
	if((*HEAD) != NULL){
		
		assignLinkedListToArray(*HEAD, *TAIL, data);
		
		if( (*sortStatus) == 0){
			prosesSorting(data, jumlah, jenis, urutan);
			assignArrayToLinkedList(HEAD, TAIL, data);
			(*sortStatus) = true;
		}
		return jumlah;
	}
	return 0;
}


void UbahProduk(int index_search, string kategori_cari){
	system("cls");	
	hiasan();
	if (kategori_cari == "makanan" 
	 || kategori_cari == "minuman" 
	 || kategori_cari == "obat" 
	 || kategori_cari == "atk" 
	 || kategori_cari == "perabot"
	 || kategori_cari == "elektronik" 
	 || kategori_cari == "peralatanbayi"){
		string input_ubah, ubah;
		RincianProduk(kategori_cari,++index_search);
		gotoxy(23, 7);  cout << "+======================================================================+" << endl;
		gotoxy(23, 8);  cout << "| 1. Ubah Nama Produk                                                  |" << endl;
		gotoxy(23, 9);  cout << "| 2. Ubah Harga Produk                                                 |" << endl;
		gotoxy(23, 10); cout << "| 3. Ubah Stok Produk                                                  |" << endl;
		gotoxy(23, 11); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(23, 12); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(23, 13); cout << "+======================================================================+" << endl;
		gotoxy(23, 14); cout << " Masukan pilihan anda : "; 
		
		getline(cin,input_ubah);
		fflush(stdin);
		
		bool ubah_berhasil;
		
		if ( input_ubah == "1" ){
			gotoxy(23, 15); cout << " Nama produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			if (kategori_cari == "makanan"){
				getProduk(index_search, HEAD_Makanan)->nama = ubah;
				sortNama_Makanan = false;
				
			} else if (kategori_cari == "minuman"){
				getProduk(index_search, HEAD_Minuman)->nama = ubah;
				sortNama_Minuman = false;
	
			} else if (kategori_cari == "obat"){
				getProduk(index_search, HEAD_Obat)->nama = ubah;
				sortNama_Obat = false;
				
			} else if (kategori_cari == "atk"){
				getProduk(index_search, HEAD_ATK)->nama = ubah;
				sortNama_ATK = false;
				
			} else if (kategori_cari == "perabot"){
				getProduk(index_search, HEAD_Perabot)->nama = ubah;
				sortNama_Perabot = false;
				
			} else if (kategori_cari == "elektronik"){
				getProduk(index_search, HEAD_Elektronik)->nama = ubah;
				sortNama_Elektronik = false;
				
			} else if (kategori_cari == "peralatanbayi"){
				getProduk(index_search, HEAD_PeralatanBayi)->nama = ubah;
				sortNama_PltBayi = false;
				
			}
			ubah_berhasil = true;
		}  else if ( input_ubah == "2"){
			int ubah_harga;
			gotoxy(23, 15); cout << " Harga produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			ubah_harga  = atoi ( ubah.c_str());
			if ( ubah_harga < 1 ) {
				Message(11);
				sleep(2);
			} else {
				if (kategori_cari == "makanan"){
					getProduk(index_search, HEAD_Makanan)->harga = ubah_harga;
				} else if (kategori_cari == "minuman"){
					getProduk(index_search, HEAD_Minuman)->harga = ubah_harga;
				} else if (kategori_cari == "obat"){
					getProduk(index_search, HEAD_Obat)->harga = ubah_harga;
				} else if (kategori_cari == "atk"){
					getProduk(index_search, HEAD_ATK)->harga = ubah_harga;
				} else if (kategori_cari == "perabot"){
					getProduk(index_search, HEAD_Perabot)->harga = ubah_harga;
				} else if (kategori_cari == "elektronik"){
					getProduk(index_search, HEAD_Elektronik)->harga = ubah_harga;
				} else if (kategori_cari == "peralatanbayi"){
					getProduk(index_search, HEAD_PeralatanBayi)->harga = ubah_harga;
				}
				ubah_berhasil = true;
			}
			 
			
		} else if ( input_ubah == "3"){
			int ubah_stok;
			gotoxy(23, 15); cout << " Stok produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			ubah_stok  = atoi ( ubah.c_str());
			if ( ubah_stok < 1 || ubah_stok > 9999) {
				Message(12);
				sleep(2);
			} else {
				if (kategori_cari == "makanan"){
					getProduk(index_search, HEAD_Makanan)->stok = ubah_stok;
				} else if (kategori_cari == "minuman"){
					getProduk(index_search, HEAD_Minuman)->stok = ubah_stok;
				} else if (kategori_cari == "obat"){
					getProduk(index_search, HEAD_Obat)->stok = ubah_stok;
				} else if (kategori_cari == "atk"){
					getProduk(index_search, HEAD_ATK)->stok = ubah_stok;
				} else if (kategori_cari == "perabot"){
					getProduk(index_search, HEAD_Perabot)->stok = ubah_stok;
				} else if (kategori_cari == "elektronik"){
					getProduk(index_search, HEAD_Elektronik)->stok = ubah_stok;
				} else if (kategori_cari == "peralatanbayi"){
					getProduk(index_search, HEAD_PeralatanBayi)->stok = ubah_stok;
				}
				ubah_berhasil = true;
			}
		}  else if ( input_ubah != "0" ) {
			Message(1);
			sleep(2);
		}
			
		if ( ubah_berhasil == true ){
			Loading("ubah");
			Message(20);
			sleep(2);
		}
			
	} else {
		Message(77);
		sleep(2);
	}
}

void HapusProduk(int index_search, string kategori_cari){
	system("cls");
	hiasan1();
	if (kategori_cari == "makanan" 
	 || kategori_cari == "minuman" 
	 || kategori_cari == "obat" 
	 || kategori_cari == "atk" 
	 || kategori_cari == "perabot"
	 || kategori_cari == "elektronik" 
	 || kategori_cari == "peralatanbayi" ){
		string input, konfir;
		RincianProduk(kategori_cari,++index_search);
		
		gotoxy(23, 7); cout << "+======================================================================+" << endl;
		gotoxy(23, 8); cout << "| 1. Hapus produk                                                      |" << endl;
		gotoxy(23, 9); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(23, 10); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(23, 11); cout << "+======================================================================+" << endl;
		gotoxy(23, 12); cout << " Masukan pilihan anda : "; 
		getline (cin, input );
		fflush(stdin);
		
		if ( input == "1" ) {
			gotoxy(23, 13); cout << "  PENGHAPUSAN INI TIDAK DAPAT DIBATALKAN" << endl;
			gotoxy(23, 14); cout << "  Anda yakin ingin MENGHAPUS riwayat pembelian? (y/t): ";
			
			getline (cin,konfir); fflush(stdin);
			if (konfir == "y" || konfir == "Y" ){
				if (kategori_cari == "makanan"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Makanan, &TAIL_Makanan);
					}else if(index_search == length(HEAD_Makanan)){
						deleteDoubleLast(&HEAD_Makanan, &TAIL_Makanan);
					}else{
						deleteDoubleAt(index_search, &HEAD_Makanan);
					}
				} else if (kategori_cari == "minuman"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Minuman, &TAIL_Minuman);
					}else if(index_search == length(HEAD_Minuman)){
						deleteDoubleLast(&HEAD_Minuman, &TAIL_Minuman);
					}else{
						deleteDoubleAt(index_search, &HEAD_Minuman);
					}
				} else if (kategori_cari == "obat"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Obat, &TAIL_Obat);
					}else if(index_search == length(HEAD_Obat)){
						deleteDoubleLast(&HEAD_Obat, &TAIL_Obat);
					}else{
						deleteDoubleAt(index_search, &HEAD_Obat);
					}
				} else if (kategori_cari == "atk"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_ATK, &TAIL_ATK);
					}else if(index_search == length(HEAD_ATK)){
						deleteDoubleLast(&HEAD_ATK, &TAIL_ATK);
					}else{
						deleteDoubleAt(index_search, &HEAD_ATK);
					}
				} else if (kategori_cari == "perabot"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Perabot, &TAIL_Perabot);
					}else if(index_search == length(HEAD_Perabot)){
						deleteDoubleLast(&HEAD_Perabot, &TAIL_Perabot);
					}else{
						deleteDoubleAt(index_search, &HEAD_Perabot);
					}
				} else if (kategori_cari == "elektronik"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Elektronik, &TAIL_Elektronik);
					}else if(index_search == length(HEAD_Elektronik)){
						deleteDoubleLast(&HEAD_Elektronik, &TAIL_Elektronik);
					}else{
						deleteDoubleAt(index_search, &HEAD_Elektronik);
					}
				} else if (kategori_cari == "peralatanbayi"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
					}else if(index_search == length(HEAD_PeralatanBayi)){
						deleteDoubleLast(&HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
					}else{
						deleteDoubleAt(index_search, &HEAD_PeralatanBayi);
					}
				}
				Loading("hapus");
				Message(30);
				sleep(2);
				
			}
		} else if ( input != "0" ) {
			Message(1);
			sleep(2);
			
		}
	} else {
		Message(77);
		sleep(2);
	}
	
}

void UbahHapusSearch(string action){
	system("cls");
	hiasan1();

	string input_cari,kategori_cari;
	
	int jml_makanan = length(HEAD_Makanan),
		jml_minuman = length(HEAD_Minuman),
		jml_obat 	= length(HEAD_Obat),
		jml_atk 	= length(HEAD_ATK),
		jml_perabot = length(HEAD_Perabot),
		jml_elektronik = length(HEAD_Elektronik),
		jml_pltbayi = length(HEAD_PeralatanBayi);
	
	produk 	tmpMakanan[ jml_makanan ], tmpMinuman[ jml_minuman ], tmpObat[ jml_obat ], tmpATK[ jml_atk ],
			tmpPerabot[ jml_perabot ], tmpElektronik[ jml_elektronik ], tmpPltBayi[ jml_pltbayi ];
	
	sortirProduk( &HEAD_Makanan, 		&TAIL_Makanan, 		tmpMakanan, 	jml_makanan, 	"nama", 1, &sortNama_Makanan);
	sortirProduk( &HEAD_Minuman, 		&TAIL_Minuman,		tmpMinuman, 	jml_minuman,	"nama", 1, &sortNama_Minuman);
	sortirProduk( &HEAD_Obat, 			&TAIL_Obat,			tmpObat, 		jml_obat,		"nama", 1, &sortNama_Obat);
	sortirProduk( &HEAD_ATK, 			&TAIL_ATK,			tmpATK, 		jml_atk,		"nama", 1, &sortNama_ATK);
	sortirProduk( &HEAD_Perabot, 		&TAIL_Perabot, 		tmpPerabot, 	jml_perabot,	"nama", 1, &sortNama_Perabot);
	sortirProduk( &HEAD_Elektronik, 	&TAIL_Elektronik, 	tmpElektronik, 	jml_elektronik,	"nama", 1, &sortNama_Elektronik);
	sortirProduk( &HEAD_PeralatanBayi,&TAIL_PeralatanBayi,	tmpPltBayi, 	jml_pltbayi, 	"nama", 1, &sortNama_PltBayi);
	
	int tmp_indeks, indeks, y_pos = DaftarProduk("admin");
	
	gotoxy(23, y_pos++); cout << " Masukan nama produk : "; 
	getline(cin,input_cari);
	
	if( jml_makanan != 0){
		tmp_indeks = fibonacciSearch(tmpMakanan, 	input_cari, jml_makanan );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "makanan"; }
	}
	if( jml_minuman != 0){
		tmp_indeks = fibonacciSearch(tmpMinuman,	input_cari, jml_minuman );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "minuman"; }
	}
	if( jml_obat != 0){
		tmp_indeks = fibonacciSearch(tmpObat, 		input_cari, jml_obat );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "obat"; }
	}
	if( jml_atk != 0){
		tmp_indeks = fibonacciSearch(tmpATK, 		input_cari, jml_atk );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "atk"; }
	}
	if( jml_perabot != 0){
		tmp_indeks = fibonacciSearch(tmpPerabot, 	input_cari, jml_perabot );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "perabot"; }	
	}
	if( jml_elektronik != 0){
		tmp_indeks = fibonacciSearch(tmpElektronik,	input_cari, jml_elektronik );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "elektronik"; }
	}
	if( jml_pltbayi != 0){
		tmp_indeks = fibonacciSearch(tmpPltBayi, 	input_cari, jml_pltbayi );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "peralatanbayi"; }
	}
	
	Loading("cari");
	if ( action == "ubah"){
		UbahProduk(indeks,kategori_cari);
	} else if (action == "hapus"){
		HapusProduk(indeks,kategori_cari);
	}
	

}

void DataProduk(){
	char keluar='n';
	while(keluar=='n'){
		string input; int x_pos = 28;
		
		system("cls");
		hiasan1();
		gotoxy(x_pos, 2); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 3); cout << "|                              DATA PRODUK                             |" << endl;
		gotoxy(x_pos, 4); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 5); cout << "| 1. Tambah produk                                                     |" << endl;
		gotoxy(x_pos, 6); cout << "| 2. Daftar produk                                                     |" << endl;
		gotoxy(x_pos, 7); cout << "| 3. Edit produk                                                       |" << endl;
		gotoxy(x_pos, 8); cout << "| 4. Hapus produk                                                      |" << endl;
		gotoxy(x_pos, 9); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(x_pos, 10); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(x_pos, 11); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 12); cout << " Masukan pilihan anda : "; 
		cin >> input;
		fflush(stdin);
		if (input == "1"){
			string nama;
			system("cls");
			TambahProduk();
			
		} else if (input == "2"){
			DaftarProduk("admin");
			
		} else if (input == "3"){
			UbahHapusSearch("ubah");
			
		} else if (input == "4"){
			UbahHapusSearch("hapus");
			
		} else if (input == "0"){
			keluar = 'y';
			
		} else {
			Message(1);
			sleep(2);
		}	
	}
}


void MenuPembeli(){	
	MainMenu();
}


void MenuAdmin(){
	char keluar='n';
	while(keluar=='n'){
		system("cls");
		hiasan();
		
		string input; int x_pos = 26;
		gotoxy(x_pos, 2); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 3); cout << "|                                ADMIN                                 |" << endl;
		gotoxy(x_pos, 4); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 5); cout << "|  1. Data produk                                                      |" << endl;
		gotoxy(x_pos, 6); cout << "|  2. Riwayat pembelian per- 7 hari                                    |" << endl;
		gotoxy(x_pos, 7); cout << "|  3. Logout                                                           |" << endl;
		gotoxy(x_pos, 8); cout << "|  4. Keluar dari sistem                                               |" << endl;
		gotoxy(x_pos, 9); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 10); cout << " Masukan pilihan anda : "; cin >> input;
		fflush(stdin);
		if (input == "1"){
			DataProduk();
			
		} else if (input == "2"){
			RiwayatPembeli(TAIL_Pembeli);
			
		} else if (input == "3"){
			keluar='y';
			SaveDataOnExit();
			MainMenu();
			
		} else if (input == "4"){
			keluar='y';
			Loading("exit");
			Message(99);
			sleep(2);
		} else {
			Message(1);
			sleep(2);
		}
	}
}


int kesempatan=3;
bool login = false;

void LoginAdmin(){	
	
	while (login == false){	
		string id, pass;
		system ("cls");
		
		gotoxy(40, 3); cout <<"+-----------------------------------------------+" << endl;
		gotoxy(88, 4); cout <<"|"; gotoxy(40, 4); cout <<"| Kesempatan login :  " << kesempatan << endl;
		gotoxy(40, 5); cout <<"|-----------------------------------------------|"<< endl;
		
		gotoxy(40, 8); cout <<"+-----------------------------------------------+"<< endl;
		gotoxy(86, 6); cout <<"] |"; gotoxy(40, 6); cout <<"| Masukkan Username : [ "; cin >> id;
		gotoxy(86, 7); cout <<"] |"; gotoxy(40, 7); cout <<"| Masukkan Password : [ "; cin >> pass;
		
		Loading("login");
		
		if( id=="admin" && pass=="admin" ){
			login = true ;
			
			Message(90);
			sleep(2);
			MenuAdmin();
			
			
		}else{
			kesempatan-=1;
			
			Message(88);
			sleep(2);
		}
		
		if(kesempatan == 0){
			Message(89);
			sleep(2);
			MainMenu();
		}
	}
}

void MainMenu(){
	char keluar='n';
	while(keluar=='n'){
		system("cls");
		string input;
		
		Opening();
		system("cls");
		system("color 17");
		gotoxy(26, 3); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 4); cout << "|        Selamat Datang di Aplikasi Konter HP Team Secret [TS]         |" << endl;
		gotoxy(26, 5); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 6); cout << "|  1. Admin                                                            |" << endl;
		gotoxy(26, 7); cout << "|  2. Pembeli                                                          |" << endl;
		gotoxy(26, 8); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 9); cout << " Siapakah anda : "; cin >> input;
		fflush(stdin);
		if (input == "1"){
			if ( kesempatan > 0){
				keluar = 'y';
				login = false;
				LoginAdmin();
			} else {
				Message(89);
				sleep(2);
				keluar = 'y';
				MainMenu();
			}
			
			
		} else if (input == "2"){
			keluar = 'y';
			MenuPembeli();
			
		}  else {
			Message(1);
			sleep(2);
		}
	}
}



int main(){
	Loadout();
	DeteksiResetRiwayat();
	//MainMenu();
	
	MenuAdmin();
	
	return 0;
}

